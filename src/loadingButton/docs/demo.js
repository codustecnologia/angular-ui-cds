angular.module('ui.codus.demo').controller('LoadingButtonDemoCtrl', function ($scope, $timeout) {
  $scope.contentSubmiting = false;

  $scope.buttonClicked = function() {
    $scope.contentSubmiting = true;
    $timeout(function() {
      $scope.contentSubmiting = false;
    }, 2000);
  };

});
