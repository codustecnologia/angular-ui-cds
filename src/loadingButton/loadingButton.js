angular.module('ui.codus.loadingButton', [])

/*
  When compiling the directive we will create a sibling <a>
  that has the same class and style.
  When the attribute value changes to true we will show the sibling
*/
.directive('cdsIsLoadingIf', function () {
  return {
    restrict: 'A',
    replace: false, 
    compile: function compile(element, attrs) {
      var replaceButton = angular.element("<a></a>");
      var replaceText = (element.text() == '') ? element.attr('value') : element.text();
      replaceButton.text(replaceText);
      replaceButton.attr('class', element.attr('class'));
      replaceButton.addClass("cds-is-loading-replace-button");
      replaceButton.attr('style', element.attr('style'));
      replaceButton.css('cursor', 'wait');
      replaceButton.prepend("<i class='fa fa-spinner fa-spin glyphicon glyphicon-refresh' style='margin-right: 5px;'></i>");
      replaceButton.hide();
      replaceButton.insertAfter(element);

      return function postLink(scope, originalButton, iAttrs) {  
        scope.$watch(iAttrs.cdsIsLoadingIf, function cdsIsLoadingIfAction(value) {
          if (value) {
            originalButton.hide();
            replaceButton.show();
          } else {
            originalButton.show();
            replaceButton.hide();
          }
        });
      };
    }
  };
});
