describe('loadingButton', function () {
  var $rootScope,
      $scope,
      $compile,
      $body,
      htmlToTest = "<button cds-is-loading-if='contentSubmiting' class='class1 class2 class3' style='float: left; margin-top: 4px;'>My Button</button>",
      $replaceBtn,
      $originalBtn;

  beforeEach(function() { 
    module('ui.codus.loadingButton');
    $body = $('body');
    $body.html('');

    inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $scope = $rootScope.$new();
      $scope.contentSubmiting = false;
      $compile = $injector.get('$compile');
    });

    $originalBtn = angular.element(htmlToTest);
    $body.append($originalBtn);
    $compile($originalBtn)($scope);
    $scope.$digest();
    $replaceBtn = $body.find(".cds-is-loading-replace-button").first();
  });

  describe("creates a sibling element visually equal to the original", function() {

    it('has is an <a> element', function() {
      expect($replaceBtn.prop('nodeName')).toBe("A");
    });

    it('copies the original class', function() {
      expect($replaceBtn.attr("class")).toMatch("class1 class2 class3 cds-is-loading-replace-button");
    });

    it('copies the original styles', function() {
      expect($replaceBtn.attr("style")).toMatch("float: left; margin-top: 4px;");
      
    });
    
    it('has a loading icon', function() {
      expect($replaceBtn.find("i").length).toBe(1);
    });

    describe('copying the text', function() {
      it("if the original is an <a>", function() {
        $body = $('body');
        $body.html('');
        $scope = $rootScope.$new();
        newHtmlToTest = "<a cds-is-loading-if='contentSubmiting'>My A</button>";
        el = angular.element(newHtmlToTest);
        $body.append(el);
        $compile(el)($scope);
        $scope.$digest();
        $replaceBtn = $body.find(".cds-is-loading-replace-button").last();
        expect($replaceBtn.text()).toBe("My A");
      });

      it("if the original is a <button>", function() {
        expect($replaceBtn.text()).toBe("My Button");
      });

      it("if the original is a <input type='submit'>", function() {
        $body = $('body');
        $body.html('');
        $scope = $rootScope.$new();
        newHtmlToTest = "<inpu cds-is-loading-if='contentSubmiting' type='submit' value='my val'></input>";
        el = angular.element(newHtmlToTest);
        $body.append(el);
        $compile(el)($scope);
        $scope.$digest();
        $replaceBtn = $body.find(".cds-is-loading-replace-button").last();
        expect($replaceBtn.text()).toBe("my val");
      });
    });
  });
  
  it("switches buttons when attribute changes", function() {
    $scope.contentSubmiting = false;
    $scope.$digest();
    expect($replaceBtn).toBeHidden();
    expect($originalBtn).not.toBeHidden();
    $scope.contentSubmiting = true;
    $scope.$digest();
    expect($replaceBtn).not.toBeHidden();
    expect($originalBtn).toBeHidden();
    $scope.contentSubmiting = false;
    $scope.$digest();
    expect($replaceBtn).toBeHidden();
    expect($originalBtn).not.toBeHidden();
  });

});
